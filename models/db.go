package models

import (
	"database/sql"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var db *sql.DB

func ConnectDatabase() *sql.DB {
	//DB Connect
	var err error
	db, err = sql.Open("mysql", "db사용자이름:db비밀번호@db이름(127.0.0.1:3306)/tmp?parseTime=true")
	if err != nil {
		panic("failed to connect database")
	}
	return db
}
