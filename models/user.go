package models

// 사용자의 정보를 저장한 model
type PersonInfo struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	DeptName string `json:"dept_name"`
}
