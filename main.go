package main

import (
	"log"
	"net/http"
	"strconv"
	"workspace/FrameWorkTest_Gin/controllers"
	"workspace/FrameWorkTest_Gin/models"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	//데이터베이스 연결
	models.ConnectDatabase()

	v1 := router.Group("/")
	{
		// 입력된 모든 정보 조회
		v1.GET("/infos", func(c *gin.Context) {
			persons := controllers.Get()
			c.JSON(http.StatusOK, gin.H{
				"result": persons,
			})

		})
		// 입력한 정보 저장
		v1.POST("/infos", func(c *gin.Context) {
			p := models.PersonInfo{Name: c.PostForm("name"), DeptName: c.PostForm("dept_name")}
			c.Bind(&p)
			controllers.Add(p)
			c.JSON(http.StatusOK, gin.H{
				"message": p,
			})
		})
		// 해당 id의 정보 수정
		v1.PUT("/change/:id", func(c *gin.Context) {
			id := c.Param("id")
			ID, err := strconv.Atoi(id)
			if err != nil {
				log.Fatalln(err)
			}
			p := models.PersonInfo{Name: c.PostForm("name")}
			p.ID = ID
			c.Bind(&p)
			controllers.Update(p)
			c.JSON(http.StatusOK, gin.H{
				"message": p,
			})
		})
		// 해당 id의 정보 삭제
		v1.DELETE("/delete/:id", func(c *gin.Context) {
			id := c.Param("id")
			ID, err := strconv.Atoi(id)
			if err != nil {
				log.Fatalln(err)
			}
			var p models.PersonInfo
			p.ID = ID
			c.Bind(&p)
			controllers.Delete(p)

			c.JSON(http.StatusOK, gin.H{
				"message": p,
			})
		})
	}
	router.Run()
}
