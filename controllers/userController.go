package controllers

import (
	"database/sql"
	"workspace/FrameWorkTest_Gin/models"
)

// 사용자의 정보를 불러오는 함수
func Get() []models.PersonInfo {
	var infos []models.PersonInfo = make([]models.PersonInfo, 0)

	rows, err := models.ConnectDatabase().Query("SELECT id, name, dept_name FROM empInfo")
	if err != nil {
		print(err)
	}
	for rows.Next() {
		var employee models.PersonInfo
		rows.Scan(&employee.ID, &employee.Name, &employee.DeptName)
		infos = append(infos, employee)
	}
	return infos
}

// 사용자의 정보를 저장하는 함수
func Add(p models.PersonInfo) *sql.Stmt {
	rows, err := models.ConnectDatabase().Prepare("INSERT INTO empInfo(name, dept_name) VALUES (?, ?)")
	if err != nil {
		print(err)
	}
	rows.Exec(p.Name, p.DeptName)
	if err != nil {
		print(err)
	}

	defer rows.Close()
	return rows
}

// 사용자의 정보를 갱신하는 함수
func Update(p models.PersonInfo) {
	stmt, err := models.ConnectDatabase().Prepare("UPDATE empInfo SET name=?, dept_name=? WHERE id=?")
	stmt.Exec(p.Name, p.DeptName, p.ID)
	if err != nil {
		return
	}

	defer stmt.Close()
	return
}

// 사용자의 정보를 삭제하는 함수
func Delete(p models.PersonInfo) {
	stmt, err := models.ConnectDatabase().Prepare("DELETE FROM empInfo WHERE id=?")
	stmt.Exec(p.ID)
	if err != nil {
		return
	}

	defer stmt.Close()
	return
}
